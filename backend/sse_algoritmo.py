#!/usr/bin/env python
# -*- coding: utf-8 -*-


# ####################################################################################
# Importar bibliotecas
# ####################################################################################
import json
import pdb
import time
import datetime
import pymongo
import asyncio
import aiohttp
from aiohttp import web
from bson import json_util, ObjectId
import json
from aiohttp_sse import sse_response
import aiohttp_cors


# ####################################################################################
# Conexao com o banco
# ####################################################################################
conexao = pymongo.MongoClient("mongodb+srv://alunos:projetointegrador@projectgo-pujmv.mongodb.net/go?retryWrites=true")
mydb = conexao['go']

frentes = []
caminhoes = []

# history1 = {
#     'id': 1, 
#     'distancia': 40, 
#     'prioridade': 'baixa', 
#     'caminhao': 0, 
#     'proximo': True,
#     'ultimo_envio': datetime.datetime.now().time().isoformat(),
#     'enchimento': 20,
#     'info': {
#         'meta': 0,
#         'toneladaHora': 0,
#         'qtdViagem': 0,
#         'toneladasBuscadas': 0
#     }
# }

# mydb.history_frentes.insert_one(history1).inserted_id

# ####################################################################################
# Funcoes
# ####################################################################################
def caminhoes_def(request):
    caminhoes = []
    print('Caminhoes')
    caminhoes_banco = mydb.caminhoes.find()
    for caminhao in caminhoes_banco:
            caminhoes.append(caminhao)
    return web.Response(text=json_util.dumps(caminhoes))

def frentes_def(request):
    frentes = []
    print('Frentes')
    frentes_banco = mydb.frentes.find()
    for frente in frentes_banco:
            frentes.append(frente)
    return web.Response(text=json_util.dumps(frentes))

history_frente_1 = []
async def sse_envio_frente_1(request):
    print('SSE Proximo')
    loop = asyncio.get_event_loop()
    async with sse_response(request) as resp: 
        # IF ultimo caminhao ja deu horario de mandar outro | resp.send = mandar caminhao
        # IF ultimo caminhao nao deu horario | resp.send = segundos que faltam   
        while True:
            history_frente_1 = mydb.history_frentes.find({'id': 1})
            # pdb.set_trace()
            
            num = mydb.history_frentes.count_documents({'id': 1})

            try:
                mandar_caminhao = history_frente_1[num-1]
            except:
                pass

            # pdb.set_trace()

            if(num == 0):
                data = 'Mandar caminhão'
                await asyncio.sleep(1)
                await resp.send(data)
            
            else:

                if(mandar_caminhao['proximo'] == True):
                    data = 'Mandar caminhão'
                    await asyncio.sleep(1)
                    await resp.send(data)
                
                else:
                    distancia = mandar_caminhao['distancia']
                    enchimento = mandar_caminhao['enchimento']

                    envio = mandar_caminhao['ultimo_envio']
                    envio_h = envio.split('.')[0].split(':')[0]
                    envio_m = envio.split('.')[0].split(':')[1]
                    envio_s = envio.split('.')[0].split(':')[2]
                    
                    data_agora = datetime.datetime.now().time().isoformat()
                    data_agora_h = data_agora.split('.')[0].split(':')[0]
                    data_agora_m = data_agora.split('.')[0].split(':')[1]
                    data_agora_s = data_agora.split('.')[0].split(':')[2]

                    hora =  int(envio_h) - int(data_agora_h)
                    if(hora < 0):
                        hora = hora + 1
                    minuto  =  int(envio_m) - int(data_agora_m)
                    if(minuto < 0):
                        minuto = abs(minuto)
                    segundo =  (60 - int(envio_s)) + int(data_agora_s)

                    tempo_maximo = (hora * 60) + minuto

                    # tempo = distancia + enchimento
                    tempo = distancia - enchimento

                    # pdb.set_trace()

                    if((tempo - tempo_maximo) > 0):
                        tempo_restante = (tempo - tempo_maximo) * 60
                        while (tempo_restante >= 0):
                            # print(tempo_restante)
                            
                            await asyncio.sleep(1)
                            if(tempo_restante == 0):
                                data = 'Mandar caminhão'
                                await resp.send(data)
                            else:
                                await resp.send(str(tempo_restante))
                            tempo_restante = tempo_restante - 1
                    
                    else:
                        data = 'Mandar caminhão'
                        await asyncio.sleep(1)
                        await resp.send(data)
    return web.Response(text=('ok'))

history_frente_2 = []
async def sse_envio_frente_2(request):
    print('SSE Proximo')
    loop = asyncio.get_event_loop()
    async with sse_response(request) as resp: 
        # IF ultimo caminhao ja deu horario de mandar outro | resp.send = mandar caminhao
        # IF ultimo caminhao nao deu horario | resp.send = segundos que faltam   
        while True:
            history_frente_2 = mydb.history_frentes.find({'id': 2})
            # pdb.set_trace()
            
            num = mydb.history_frentes.count_documents({'id': 2})

            try:
                mandar_caminhao = history_frente_2[num-1]
            except:
                pass

            # pdb.set_trace()

            if(num == 0):
                data = 'Mandar caminhão'
                await asyncio.sleep(1)
                await resp.send(data)
            
            else:

                if(mandar_caminhao['proximo'] == True):
                    data = 'Mandar caminhão'
                    await asyncio.sleep(1)
                    await resp.send(data)
                
                else:
                    distancia = mandar_caminhao['distancia']
                    enchimento = mandar_caminhao['enchimento']

                    envio = mandar_caminhao['ultimo_envio']
                    envio_h = envio.split('.')[0].split(':')[0]
                    envio_m = envio.split('.')[0].split(':')[1]
                    envio_s = envio.split('.')[0].split(':')[2]
                    
                    data_agora = datetime.datetime.now().time().isoformat()
                    data_agora_h = data_agora.split('.')[0].split(':')[0]
                    data_agora_m = data_agora.split('.')[0].split(':')[1]
                    data_agora_s = data_agora.split('.')[0].split(':')[2]

                    hora =  int(envio_h) - int(data_agora_h)
                    if(hora < 0):
                        hora = hora + 1
                    minuto  =  int(envio_m) - int(data_agora_m)
                    if(minuto < 0):
                        minuto = abs(minuto)
                    segundo =  (60 - int(envio_s)) + int(data_agora_s)

                    tempo_maximo = (hora * 60) + minuto

                    # tempo = distancia + enchimento
                    tempo = distancia - enchimento

                    # pdb.set_trace()

                    if((tempo - tempo_maximo) > 0):
                        tempo_restante = (tempo - tempo_maximo) * 60
                        while (tempo_restante >= 0):
                            # print(tempo_restante)
                            
                            await asyncio.sleep(1)
                            if(tempo_restante == 0):
                                data = 'Mandar caminhão'
                                await resp.send(data)
                            else:
                                await resp.send(str(tempo_restante))
                            tempo_restante = tempo_restante - 1
                    
                    else:
                        data = 'Mandar caminhão'
                        await asyncio.sleep(1)
                        await resp.send(data)
    return web.Response(text=('ok'))

history_frente_3 = []
async def sse_envio_frente_3(request):
    print('SSE Proximo')
    loop = asyncio.get_event_loop()
    async with sse_response(request) as resp: 
        # IF ultimo caminhao ja deu horario de mandar outro | resp.send = mandar caminhao
        # IF ultimo caminhao nao deu horario | resp.send = segundos que faltam   
        while True:
            history_frente_3 = mydb.history_frentes.find({'id': 3})
            # pdb.set_trace()
            
            num = mydb.history_frentes.count_documents({'id': 3})

            try:
                mandar_caminhao = history_frente_3[num-1]
            except:
                pass

            # pdb.set_trace()

            if(num == 0):
                data = 'Mandar caminhão'
                await asyncio.sleep(1)
                await resp.send(data)
            
            else:

                if(mandar_caminhao['proximo'] == True):
                    data = 'Mandar caminhão'
                    await asyncio.sleep(1)
                    await resp.send(data)
                
                else:
                    distancia = mandar_caminhao['distancia']
                    enchimento = mandar_caminhao['enchimento']

                    envio = mandar_caminhao['ultimo_envio']
                    envio_h = envio.split('.')[0].split(':')[0]
                    envio_m = envio.split('.')[0].split(':')[1]
                    envio_s = envio.split('.')[0].split(':')[2]
                    
                    data_agora = datetime.datetime.now().time().isoformat()
                    data_agora_h = data_agora.split('.')[0].split(':')[0]
                    data_agora_m = data_agora.split('.')[0].split(':')[1]
                    data_agora_s = data_agora.split('.')[0].split(':')[2]

                    hora =  int(envio_h) - int(data_agora_h)
                    if(hora < 0):
                        hora = hora + 1
                    minuto  =  int(envio_m) - int(data_agora_m)
                    if(minuto < 0):
                        minuto = abs(minuto)
                    segundo =  (60 - int(envio_s)) + int(data_agora_s)

                    tempo_maximo = (hora * 60) + minuto

                    # tempo = distancia + enchimento
                    tempo = distancia - enchimento

                    # pdb.set_trace()

                    if((tempo - tempo_maximo) > 0):
                        tempo_restante = (tempo - tempo_maximo) * 60
                        while (tempo_restante >= 0):
                            # print(tempo_restante)
                            
                            await asyncio.sleep(1)
                            if(tempo_restante == 0):
                                data = 'Mandar caminhão'
                                await resp.send(data)
                            else:
                                await resp.send(str(tempo_restante))
                            tempo_restante = tempo_restante - 1
                    
                    else:
                        data = 'Mandar caminhão'
                        await asyncio.sleep(1)
                        await resp.send(data)
    return web.Response(text=('ok'))

history_frente_4 = []
async def sse_envio_frente_4(request):
    print('SSE Proximo')
    loop = asyncio.get_event_loop()
    async with sse_response(request) as resp: 
        # IF ultimo caminhao ja deu horario de mandar outro | resp.send = mandar caminhao
        # IF ultimo caminhao nao deu horario | resp.send = segundos que faltam   
        while True:
            history_frente_4 = mydb.history_frentes.find({'id': 4})
            # pdb.set_trace()
            
            num = mydb.history_frentes.count_documents({'id': 4})

            try:
                mandar_caminhao = history_frente_4[num-1]
            except:
                pass

            # pdb.set_trace()

            if(num == 0):
                data = 'Mandar caminhão'
                await asyncio.sleep(1)
                await resp.send(data)
            
            else:

                if(mandar_caminhao['proximo'] == True):
                    data = 'Mandar caminhão'
                    await asyncio.sleep(1)
                    await resp.send(data)
                
                else:
                    distancia = mandar_caminhao['distancia']
                    enchimento = mandar_caminhao['enchimento']

                    envio = mandar_caminhao['ultimo_envio']
                    envio_h = envio.split('.')[0].split(':')[0]
                    envio_m = envio.split('.')[0].split(':')[1]
                    envio_s = envio.split('.')[0].split(':')[2]
                    
                    data_agora = datetime.datetime.now().time().isoformat()
                    data_agora_h = data_agora.split('.')[0].split(':')[0]
                    data_agora_m = data_agora.split('.')[0].split(':')[1]
                    data_agora_s = data_agora.split('.')[0].split(':')[2]

                    hora =  int(envio_h) - int(data_agora_h)
                    if(hora < 0):
                        hora = hora + 1
                    minuto  =  int(envio_m) - int(data_agora_m)
                    if(minuto < 0):
                        minuto = abs(minuto)
                    segundo =  (60 - int(envio_s)) + int(data_agora_s)

                    tempo_maximo = (hora * 60) + minuto

                    # tempo = distancia + enchimento
                    tempo = distancia - enchimento

                    # pdb.set_trace()

                    if((tempo - tempo_maximo) > 0):
                        tempo_restante = (tempo - tempo_maximo) * 60
                        while (tempo_restante >= 0):
                            # print(tempo_restante)
                            
                            await asyncio.sleep(1)
                            if(tempo_restante == 0):
                                data = 'Mandar caminhão'
                                await resp.send(data)
                            else:
                                await resp.send(str(tempo_restante))
                            tempo_restante = tempo_restante - 1
                    
                    else:
                        data = 'Mandar caminhão'
                        await asyncio.sleep(1)
                        await resp.send(data)
    return web.Response(text=('ok'))


history_frente_5 = []
async def sse_envio_frente_5(request):
    print('SSE Proximo')
    loop = asyncio.get_event_loop()
    async with sse_response(request) as resp: 
        # IF ultimo caminhao ja deu horario de mandar outro | resp.send = mandar caminhao
        # IF ultimo caminhao nao deu horario | resp.send = segundos que faltam   
        while True:
            history_frente_5 = mydb.history_frentes.find({'id': 5})
            # pdb.set_trace()
            
            num = mydb.history_frentes.count_documents({'id': 5})
            
            try:
                mandar_caminhao = history_frente_5[num-1]
            except:
                pass

            # pdb.set_trace()

            if(num == 0):
                data = 'Mandar caminhão'
                await asyncio.sleep(1)
                await resp.send(data)
            
            else:

                if(mandar_caminhao['proximo'] == True):
                    data = 'Mandar caminhão'
                    await asyncio.sleep(1)
                    await resp.send(data)
                
                else:
                    distancia = mandar_caminhao['distancia']
                    enchimento = mandar_caminhao['enchimento']

                    envio = mandar_caminhao['ultimo_envio']
                    envio_h = envio.split('.')[0].split(':')[0]
                    envio_m = envio.split('.')[0].split(':')[1]
                    envio_s = envio.split('.')[0].split(':')[2]
                    
                    data_agora = datetime.datetime.now().time().isoformat()
                    data_agora_h = data_agora.split('.')[0].split(':')[0]
                    data_agora_m = data_agora.split('.')[0].split(':')[1]
                    data_agora_s = data_agora.split('.')[0].split(':')[2]

                    hora =  int(envio_h) - int(data_agora_h)
                    if(hora < 0):
                        hora = hora + 1
                    minuto  =  int(envio_m) - int(data_agora_m)
                    if(minuto < 0):
                        minuto = abs(minuto)
                    segundo =  (60 - int(envio_s)) + int(data_agora_s)

                    tempo_maximo = (hora * 60) + minuto

                    # tempo = distancia + enchimento
                    tempo = distancia - enchimento

                    # pdb.set_trace()

                    if((tempo - tempo_maximo) > 0):
                        tempo_restante = (tempo - tempo_maximo) * 60
                        while (tempo_restante >= 0):
                            # print(tempo_restante)
                            
                            await asyncio.sleep(1)
                            if(tempo_restante == 0):
                                data = 'Mandar caminhão'
                                await resp.send(data)
                            else:
                                await resp.send(str(tempo_restante))
                            tempo_restante = tempo_restante - 1
                    
                    else:
                        data = 'Mandar caminhão'
                        await asyncio.sleep(1)
                        await resp.send(data)
    return web.Response(text=('ok'))
# ####################################################################################
# CORS APP
# ####################################################################################
loop = asyncio.get_event_loop()
app = web.Application(loop=loop)

cors = aiohttp_cors.setup(app, defaults={
    "*": aiohttp_cors.ResourceOptions(
        allow_credentials=True,
        expose_headers="*",
        allow_headers="*",
    )
})


# ####################################################################################
# Routers
# ####################################################################################
caminhoes_router = cors.add(app.router.add_resource("/caminhoes"))
cors.add(caminhoes_router.add_route("GET", caminhoes_def))

frentes_router = cors.add(app.router.add_resource("/frentes"))
cors.add(frentes_router.add_route("GET", frentes_def))

frente1_router = cors.add(app.router.add_resource("/frente1"))
cors.add(frente1_router.add_route("GET", sse_envio_frente_1))

frente2_router = cors.add(app.router.add_resource("/frente2"))
cors.add(frente2_router.add_route("GET", sse_envio_frente_2))

frente3_router = cors.add(app.router.add_resource("/frente3"))
cors.add(frente3_router.add_route("GET", sse_envio_frente_3))

frente4_router = cors.add(app.router.add_resource("/frente4"))
cors.add(frente4_router.add_route("GET", sse_envio_frente_4))

frente5_router = cors.add(app.router.add_resource("/frente5"))
cors.add(frente5_router.add_route("GET", sse_envio_frente_5))

web.run_app(app)