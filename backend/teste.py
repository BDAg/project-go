import asyncio
import datetime
import aiohttp
from aiohttp import web
import json

# async def display_date(loop):
#     end_time = loop.time() + 5.0
#     while True:
#         print(datetime.datetime.now())
#         if (loop.time() + 1.0) >= end_time:
#             break
#         await asyncio.sleep(1)

async def sse_envio(request, loop):
    end_time = loop.time() + 5.0
    while True:
        print(datetime.datetime.now())
        if (loop.time() + 1.0) >= end_time:
            break
        await asyncio.sleep(1)
        # response_obj = { 'status' : 'success' }
        # return web.Response(text=json.dumps(response_obj))

async def handle(request, loop):
    response_obj = { 'status' : 'success' }
    return web.Response(text=json.dumps(response_obj))

loop = asyncio.get_event_loop()
loop.run_until_complete(sse_envio(loop))
loop.close()

app = web.Application()
app.router.add_get('/', handle)
app.router.add_get('/sse-envio', sse_envio)

web.run_app(app)