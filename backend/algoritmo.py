#!/usr/bin/env python
# -*- coding: utf-8 -*-


# ####################################################################################
# Importar bibliotecas
# ####################################################################################
import json
import pdb
import time
import datetime
import pymongo
import asyncio
import aiohttp
from aiohttp import web
from bson import json_util, ObjectId
import json
from aiohttp_sse import sse_response
import aiohttp_cors

# ####################################################################################
# Conexao com o banco
# ####################################################################################

conexao = pymongo.MongoClient("mongodb+srv://alunos:projetointegrador@projectgo-pujmv.mongodb.net/go?retryWrites=true")
# pdb.set_trace()
mydb = conexao['go']

frentes = []
caminhoes = []

def caminhoes_def(request):
    caminhoes_banco = []
    print('Caminhoes')
    caminhoes_banco = mydb.caminhoes.find()
    for caminhao in caminhoes_banco:
            caminhoes.append(caminhao)
    return web.Response(text=json_util.dumps(caminhoes))

def frentes_def(request):
    frentes_banco = []
    print('Frentes')
    frentes_banco = mydb.frentes.find()
    for frente in frentes_banco:
            frentes.append(frente)
    return web.Response(text=json_util.dumps(frentes))

async def sse_envio(request):
    loop = asyncio.get_event_loop()

    print('SSE Proximo')
    

    async with sse_response(request) as resp:
        while True:
            print(datetime.datetime.now())
            data = {'data': datetime.datetime.now()}
            await asyncio.sleep(1)
            await resp.send('data')
    
    return web.Response(text=('ok'))

loop = asyncio.get_event_loop()
app = web.Application(loop=loop)

cors = aiohttp_cors.setup(app, defaults={
    "*": aiohttp_cors.ResourceOptions(
        allow_credentials=True,
        expose_headers="*",
        allow_headers="*",
    )
})

# app.router.add_get('/caminhoes', caminhoes_def)
# app.router.add_get('/frentes', frentes_def)
# app.router.add_get('/sse-envio', sse_envio)

caminhoes_router = cors.add(app.router.add_resource("/caminhoes"))
cors.add(caminhoes_router.add_route("GET", caminhoes_def))

frentes_router = cors.add(app.router.add_resource("/frentes"))
cors.add(frentes_router.add_route("GET", frentes_def))

web.run_app(app)

# pdb.set_trace()
# for frente in frentes:
#     print(frente)
#     pdb.set_trace()

# pdb.set_trace()


# ####################################################################################
# Explicação do objeto json dos caminhoes
# 
# placa: Identificador dos caminhoes
# frente: Id da frente que ele esta | 0 = Nenhuma frente
# status: aguardando = Esperanddo frente para ir | indo = Indo para frente | enchendo = Enchendo caminho na frente | Voltando = Voltando da frente
# info: Previsoes dos horarios de acordo com os calculos feitos com os dados das frentes | info = 0 significa que esta aguardando
# ####################################################################################
caminhoes_json = [
    {
        'placa': 'azs-123', 
        'frente': 1, 
        'status': 'indo', 
        'info': {
            'saida': '12:00',
            'enchimento': '12:50',
            'chegada': '13:50'
        }
    },

    {
        'placa': 'jag-111', 
        'frente': 0, 
        'status': 'aguardando',
        'info': {
            'saida': 0,
            'enchimento': 0,
            'chegada': 0
        }
    },

    {
        'placa': 'bzs-237', 
        'frente': 0, 
        'status': 'aguardando',
        'info': {
            'saida': 0,
            'enchimento': 0,
            'chegada': 0
        }
    },

    {
        'placa': 'gfs-777', 
        'frente': 0, 
        'status': 'aguardando',
        'info': {
            'saida': 0,
            'enchimento': 0,
            'chegada': 0
        }
    },

    {
        'placa': 'kzs-453', 
        'frente': 0, 
        'status': 'aguardando',
        'info': {
            'saida': 0,
            'enchimento': 0,
            'chegada': 0
        }
    },
]


# ####################################################################################
# Explicação do objeto json das frentes
# 
# id: Identificador das frentes
# distancia: Calculada em minutos
# enchimento: Minutos que demora para um caminhao encher nessa frente | Muda automaticamente de acordo com o momento
# prioridade: Pode ser alterada de acordo com o momento = Baixa, Media, Alta
# caminhao: 0 = Nao tem nenhum caminhao na frente | Numero da placa = Identificador do caminhao 
# proximo: True = Mandar outro caminhao | False = Nao mandar outro caminhao ainda
# ####################################################################################
frente1 = {
    'id': 1, 
    'distancia': 50, 
    'enchimento': 10, 
    'prioridade': 'baixa', 
    'caminhao': 0, 
    'proximo': True,
    'info': {
        'meta': 1143.75,
        'toneladaHora': 5.22,
        'qtdViagem': 2,
        'toneladasBuscadas': 0
    }
}

frente2 = {
    'id': 2, 
    'distancia': 30, 
    'prioridade': 'baixa', 
    'caminhao': 0, 
    'proximo': True,
    'info': {
        'meta': 0,
        'toneladaHora': 0,
        'qtdViagem': 0,
        'toneladasBuscadas': 0
    }
}

frente3 = {
    'id': 3, 
    'distancia': 80, 
    'prioridade': 'baixa', 
    'caminhao': 0, 
    'proximo': True,
    'info': {
        'meta': 0,
        'toneladaHora': 0,
        'qtdViagem': 0,
        'toneladasBuscadas': 0
    }
}

frente4 = {
    'id': 4, 
    'distancia': 20, 
    'prioridade': 'baixa', 
    'caminhao': 0, 
    'proximo': True,
    'info': {
        'meta': 0,
        'toneladaHora': 0,
        'qtdViagem': 0,
        'toneladasBuscadas': 0
    }
}

frente5 = {
    'id': 5, 
    'distancia': 40, 
    'prioridade': 'baixa', 
    'caminhao': 0, 
    'proximo': True,
    'info': {
        'meta': 0,
        'toneladaHora': 0,
        'qtdViagem': 0,
        'toneladasBuscadas': 0
    }
}

# pdb.set_trace()
# mydb.frentes.insert_one(frente1).inserted_id

# Isso vai ser retornado do banco
# frentes = [frente1, frente2, frente3, frente4, frente5]


# Mandar caminhao para frente
# Mandar caminhao apenas se o status estiver "aguardando"
def selecionarCaminhao(frente, caminhoes):
    numero_frente = frente['id']

    # pdb.set_trace()
    for caminhao in caminhoes:
        if caminhao['status'] == 'aguardando':
            caminhao['status'] = 'indo'
            caminhao['info']['saida'] = datetime.datetime.now().time()
            caminhao['frente'] = numero_frente
            return caminhao


# Calculo para saber se a frente precisa de caminhao 
# Return True or False
# Ver ultimo caminhao enviado na frente e fazer o calculo se ja precisa de outro
def calculoFrente(frente, caminhoes):
    for caminhao in caminhoes:
        if caminhao['frente'] == frente['id'] and caminhao['status'] != 'indo':
            frente['proximo'] = False
        
        else:
            frente['proximo'] = True


# Selecionar frente que precisa de caminhao
# Adcionar a funcao calculoFrente() no futuro
def selecionarFrente(frentes, caminhoes):

    for frente in frentes:
        # pdb.set_trace()

        # Calcular se frente precisa de caminhao
        calculoFrente(frente, caminhoes)

        if frente['proximo'] == True:
            caminhao_selecionado = selecionarCaminhao(frente, caminhoes)

            if caminhao_selecionado == None:
                print("Nenhum caminhao disponivel")
            
            else:
                frente['caminhao'] = caminhao_selecionado['placa']

                frente['proximo'] = False
                viagens = frente['info']['qtdViagem']
                frente['info']['qtdViagem'] = viagens + 1
                # Mandar requisicao url nodejs para dashboard avisando que caminhao foi enviado
                print("Mandar caminhao para frente --- ", frente['id'])
        
        else:
            print("Nenhuma frente disponivel")
            

# Function main
def main():
    while True:
        frentes_banco = mydb.frentes.find()
        for frente in frentes_banco:
            frentes.append(frente)

        caminhoes_banco = mydb.caminhoes.find()
        for caminhao in caminhoes_banco:
            caminhoes.append(caminhao)
        
        time.sleep(2)
        # pdb.set_trace()
        selecionarFrente(frentes, caminhoes)

        # Funcao para ver se caminhoes estao disponiveis


# Chamar function main
if __name__ == '__main__':
    main()