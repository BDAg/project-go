import { TestBed } from '@angular/core/testing';

import { AgroterenasService } from './agroterenas.service';

describe('AgroterenasService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AgroterenasService = TestBed.get(AgroterenasService);
    expect(service).toBeTruthy();
  });
});
