import { Component, ElementRef } from "@angular/core";
import {
  BreakpointObserver,
  Breakpoints,
  BreakpointState
} from "@angular/cdk/layout";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";
import { MediaChange, MediaObserver } from "@angular/flex-layout";

@Component({
  selector: 'app-my-nav',
  templateUrl: './my-nav.component.html',
  styleUrls: ['./my-nav.component.css']
})
export class MyNavComponent {

  opened: boolean = false;

  isHandset: Observable<BreakpointState> = this.breakpointObserver.observe(
    Breakpoints.Handset
  );

  constructor(
    private breakpointObserver: BreakpointObserver,
    private media: MediaObserver
  ) {
    this.media.media$.subscribe((mediaChange: MediaChange) => {
      this.opened = this.getOpened(mediaChange);
    });
  }

  private getOpened(mediaChange: MediaChange): boolean {
    if (this.media.isActive("gt-sm")) {
      return false;
    } else {
      return false;
    }
  }
}