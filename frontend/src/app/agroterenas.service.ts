import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AgroterenasService {

  private api = "http://localhost:8080/";

  constructor(
    private http: HttpClient,
    private router: Router,
  ) {
    console.log('Service');
   }

  getCaminhoes() {
    console.log('Lista de caminhoes')
    return this.http.get(this.api+'caminhoes')
  }

  getFrentes() {
    console.log('Lista de frentes')
    return this.http.get(this.api+'frentes')
  }
  
}
