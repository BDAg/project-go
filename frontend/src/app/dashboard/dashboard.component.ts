import { Component, NgZone } from '@angular/core';
import { map } from 'rxjs/operators';
import { AgroterenasService } from '../agroterenas.service';
import { Observable } from "rxjs";


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent {
  displayedColumnsCaminhao: string[] = ['placa', 'frente', 'status'];
  displayedColumns: string[] = ['id', 'caminhao', 'distancia', 'enchimento', 'prioridade', 'proximo'];

  public dataSource = [];
  public caminhoes: any;
  public frentes: any;

  public frente1: any;
  public frente2: any;
  public frente3: any;
  public frente4: any;
  public frente5: any;

  eventSource: any = window['EventSource'];

  constructor(
    private service: AgroterenasService,
    private zone: NgZone,
  ) {
    console.log('Dashboard');
    this.getCaminhoes()
    this.getFrentes()
    this.getFrente2()
    this.getFrente3()
    this.getFrente4()
    this.getFrente5()
  }

  getCaminhoes() {
    return this.service.getCaminhoes().subscribe(
      res => {
        this.caminhoes = res
        console.log('Caminhoes', this.caminhoes)
      },
      err => console.log('Erro', err)
    )
  }

  getFrentes() {
    return this.service.getFrentes().subscribe(
      res => {
        this.frentes = res
        console.log('Frentes', this.frentes)
      },
      err => console.log('Erro', err)
    )
  }

  getFrente2() {
    const observable = Observable.create(observer => {
      const eventSource = new EventSource('http://localhost:8080/frente2');
      eventSource.onmessage = x => observer.next(x.data);
      eventSource.onerror = x => observer.error(x);

      return () => {
        eventSource.close();
      };
    });

    observable.subscribe({
      next: guid => {
        this.zone.run(() => this.frente2 = guid);
      },
      error: err => console.error('something wrong occurred: ' + err)
    }
  )};

  getFrente3() {
    const observable = Observable.create(observer => {
      const eventSource = new EventSource('http://localhost:8080/frente3');
      eventSource.onmessage = x => observer.next(x.data);
      eventSource.onerror = x => observer.error(x);

      return () => {
        eventSource.close();
      };
    });

    observable.subscribe({
      next: guid => {
        this.zone.run(() => this.frente3 = guid);
      },
      error: err => console.error('something wrong occurred: ' + err)
    }
  )};

  getFrente4() {
    const observable = Observable.create(observer => {
      const eventSource = new EventSource('http://localhost:8080/frente4');
      eventSource.onmessage = x => observer.next(x.data);
      eventSource.onerror = x => observer.error(x);

      return () => {
        eventSource.close();
      };
    });

    observable.subscribe({
      next: guid => {
        this.zone.run(() => this.frente4 = guid);
      },
      error: err => console.error('something wrong occurred: ' + err)
    }
  )};
  
  getFrente5() {
    const observable = Observable.create(observer => {
      const eventSource = new EventSource('http://localhost:8080/frente5');
      eventSource.onmessage = x => observer.next(x.data);
      eventSource.onerror = x => observer.error(x);

      return () => {
        eventSource.close();
      };
    });

    observable.subscribe({
      next: guid => {
        this.zone.run(() => this.frente5 = guid);
      },
      error: err => console.error('something wrong occurred: ' + err)
    }
  )};

  ngOnInit() {
    const observable = Observable.create(observer => {
      const eventSource = new EventSource('http://localhost:8080/frente1');
      eventSource.onmessage = x => observer.next(x.data);
      eventSource.onerror = x => observer.error(x);

      return () => {
        eventSource.close();
      };
    });

    observable.subscribe({
      next: guid => {
        this.zone.run(() => this.frente1 = guid);
      },
      error: err => console.error('something wrong occurred: ' + err)
    }
    )};
}
